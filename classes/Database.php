<?php
require_once __DIR__ . '/../models/Account.php';
require_once __DIR__ . '/../models/Product.php';
require_once __DIR__ . '/../models/Cart.php';
require_once __DIR__ . '/../models/CartProduct.php';

define('DB_CONFIG', '/../config.json');

class Database
{
    private $server;
    private $user;
    private $password;
    private $db;
    public $conn;

    public function __construct()
    {
        if (file_exists(__DIR__ . DB_CONFIG)) {
            $config = json_decode(file_get_contents(__DIR__ . DB_CONFIG));
            $this->server = $config->server;
            $this->user = $config->user;
            $this->password = $config->password;
            $this->db = $config->database;
        } else {
            require_once '../setup.php';
            die();
        }

        $this->conn = new mysqli($this->server, $this->user, $this->password, $this->db);

        // Check connection
        if ($this->conn->connect_error) {
            die("Connection to SQL server failed: " . $conn->connect_error);
        }
    }

    public function dbname()
    {
        return $this->db;
    }

    public function __desctruct()
    {
        if (isset($this->conn)) {
            $this->conn->close();
        }

    }
}
