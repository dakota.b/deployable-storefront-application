<?php
require_once __DIR__ . '/../models/Account.php';

class Authentication
{
    public static function is_authenticated($account = null)
    {
        // TODO: authenticate users here
        if (Authentication::get_ssid() !== null) {
            return true;
        } else {
            return false;
        }
    }

    public static function get_ssid()
    {
        return array_key_exists('ssid', $_SESSION) ? $_SESSION['ssid'] : null;
    }

    public static function valid_email($email)
    {
        // email regex provided by https://emailregex.com/

        return preg_match('/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/', $email);
    }
}
