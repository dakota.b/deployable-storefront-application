<?php
class Asset
{
    public $href;
    public $integrity;
    public $crossorigin;

    public function __construct($asset)
    {
        if (is_string($asset)) {
            $this->href = $asset;
        } else {
            $this->href = $asset->href;
            $this->integrity = $asset->integrity;
            $this->crossorigin = $asset->crossorigin;
        }
    }
}
