<?php
require_once __DIR__ . '/Asset.php';

class Stylesheet extends Asset
{
    public function render()
    {
        echo '<link rel="stylesheet" href="' . $this->href . '" '
            . ($this->integrity ? 'integrity="' . $this->integrity . '" ' : '')
            . ($this->crossorigin ? 'crossorigin="' . $this->crossorigin . '" ' : '')
            . ' />';
    }

    public static function render_inline($style)
    {
        if ($style) {
            echo '<style type="text/css">' . PHP_EOL;
            echo $style;
            echo '</style>' . PHP_EOL;
        }
    }
}
