<?php
class File
{
    public $name;

    public static function upload_image($fieldname, $filename = null)
    {
        $base = __DIR__ . '/../uploads/';
        $fname = $base . basename($_FILES[$fieldname]["name"]);

        $upload_allowed = true;
        $file_ext = strtolower(pathinfo($fname, PATHINFO_EXTENSION));

        // Check if image file is parseable
        $upload_allowed = getimagesize($_FILES[$fieldname]["tmp_name"]) !== false;

        if (!isset($filename)) {
            $fname = $base . md5_file($_FILES[$fieldname]["tmp_name"]) . '.' . strtolower($file_ext);
        }

        if (move_uploaded_file($_FILES[$fieldname]["tmp_name"], $fname)) {
            return basename($fname);
        }
    }
}
