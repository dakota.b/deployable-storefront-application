<?php
include_once __DIR__ . '/Database.php';

class StaticDB
{
    public static $store;

    public static function connect_store()
    {
        if (!isset(StaticDB::$store)) {
            StaticDB::$store = new Database();
        }

    }
}
