<?php
require_once __DIR__ . '/Stylesheet.php';
require_once __DIR__ . '/Script.php';

class _Page
{
    public static $style = "";
    public static $stylesheets = [];
    public static $scripts = [];
    public static $title;

    public static function load_defaults()
    {
        $assets = json_decode(file_get_contents(__DIR__ . '/../assets/assets.json'));
        foreach ($assets->css as $stylesheet) {
            array_push(_Page::$stylesheets, new Stylesheet($stylesheet));
        }

        foreach (glob(__DIR__ . '/../assets/css/*.css') as $stylesheet) {
            array_push(_Page::$stylesheets, new Stylesheet(_Page::base_dir() . '/assets/css/' . basename($stylesheet)));
        }

        foreach ($assets->js as $script) {
            array_push(_Page::$scripts, new Script($script));
        }

        foreach (glob(__DIR__ . '/../assets/js/*.js') as $script) {
            array_push(_Page::$scripts, new Script($script));
        }
    }

    public static function base_dir()
    {
        return htmlentities(substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], '/')));
    }
}

_Page::load_defaults();
