<?php
require_once __DIR__ . '/Asset.php';

class Script extends Asset
{
    public function render()
    {
        echo '<script src="' . $this->href . '" '
            . ($this->integrity ? 'integrity="' . $this->integrity . '" ' : '')
            . ($this->crossorigin ? 'crossorigin="' . $this->crossorigin . '"' : '')
            . '></script>';
    }

    public static function render_collection(array $scripts)
    {
        echo '<div class="scripts">' . PHP_EOL;
        foreach ($scripts as $scr) {
            $scr->render();
            echo PHP_EOL;
        }
        echo '</div>' . PHP_EOL;
    }

    public static function render_inline($script)
    {
        echo '<div class="inline scripts">' . PHP_EOL;
        echo '<script>' . PHP_EOL;
        echo $script;
        echo '</script>' . PHP_EOL . '</div>' . PHP_EOL;
    }
}
