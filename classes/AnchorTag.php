<?php
class AnchorTag
{
    private $text;
    private $url;

    public function __construct($url, $text = null)
    {
        if (!isset($text)) {
            $text = $url;
        }
        $this->url = $url;
        $this->text = $text;
    }

    public function echo_nav()
    {
        echo '<a class="nav-link" href="' . $this->url . '">' . htmlspecialchars($this->text) . '</a>';
    }

    public function button_nav($post, $value)
    {
        $post = htmlspecialchars($post);
        $value = htmlspecialchars($value);

        echo <<<HTML
            <form action="{$this->url}" method="post">
                <button class="btn btn-link nav-link"
                    name="$post"
                    value="$value">
                    {$this->text}
                </button>
            </form>
HTML;
    }
}
