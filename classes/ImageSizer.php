<?php
class ImageSizer
{
    private $img;
    private $alt;

    public function __construct($imgurl, $alt = null)
    {
        $this->img = $imgurl;
    }

    public function __get($property)
    {
        if (method_exists($this, $property)) {
            return $this->$property();
        }
    }

    private function alt()
    {
        if ($this->alt) {
            return $this->alt;
        }

        return $this->img;
    }

    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }

        return $this;
    }

    public function thumbnail()
    {
        return '<img src="' . $this->img . '" alt="' . $this->alt() . '" style="max-width: 200px; max-height: 200px; />';
    }

    public function hq()
    {
        return '<img src="' . $this->img . '" alt="' . $this->alt() . '" style="max-width: 1000px; max-height: 1000px;" />';
    }

    public function raw()
    {
        return '<img src="' . $this->img . '" alt="' . $this->alt() . '" />';
    }

    public function card()
    {
        return '<img class="card-img-top product-image" src="' . $this->img . '" style="width: 100%;
        height: 15vw;
        object-fit: cover;" alt="' . $this->alt() . '" />';
    }
}
