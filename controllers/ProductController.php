<?php
include_once __DIR__ . '/../models/Product.php';
require_once __DIR__ . '/../classes/Authentication.php';
require_once __DIR__ . '/../views/src/Page.php';
require_once __DIR__ . '/../classes/File.php';
require_once __DIR__ . '/RoutingController.php';

StaticDB::connect_store();

class ProductController
{
    public function invoke()
    {
        if (isset($_GET['show'])) {
            $product = Product::get(intval($_GET['show']));
            include __DIR__ . '/../views/ProductView.php';
            ProductView::render($product);
        } elseif (isset($_GET['action'])) {
            $ac = $_GET['action'];
            if (method_exists($this, $ac)) {
                $this->$ac();
            } else {
                // TODO: throw an error
            }
        } else {
            // no special product is requested, we'll show a list of all available products
            $products = Product::get_product_list();
            include __DIR__ . '/../views/ProductListView.php';
            $plv = new ProductListView();
            $plv->render($products);
        }
    }

    public function add()
    {
        if (!Authentication::is_authenticated()) {
            Page::error("You are not properly authenticated to view this page. Please log in.");
            Page::render();
            die();
        }

        if (!empty($_POST)) {
            $keys = array_keys($_POST);
            $requirements = array('name', 'price', 'description');

            $product = new Product();

            foreach ($requirements as $req) {
                if (!in_array($req, $keys)) {
                    Page::error("You have submitted a malformed request. Please try again.");
                    Page::render();
                    die();
                }
            }

            foreach ($requirements as $key) {
                $product->$key = $_POST[$key];
            }

            $product->image = File::upload_image('image');

            $product->posted_id = Authentication::get_ssid();

            $nid = $product->add();

            header('Location: ' . RoutingController::get_route_for('product', $nid));
            die();
        } else {
            // show the form to the user
            include __DIR__ . '/../views/AddProductView.php';
            AddProductView::render();
        }
    }

    public function delete()
    {
        if (array_key_exists('delete', $_POST)) {
            $prod = Product::get($_POST['delete']);
            if ($prod && $prod->owned_by(Account::get(Authentication::get_ssid()))) {
                if ($prod->remove()) {
                    Page::notify('Product successfully deleted.');
                } else {
                    Page::error('An internal error occurred.');
                }

            } else {
                Page::error('Malformed request or insufficient permission to perform this action');
            }
            Page::render('');
            die();
        }

        Page::error('The request could not be handled as sent to the server.');
        Page::render('');
        die();
    }

}
