<?php
require_once __DIR__ . '/../models/Cart.php';
require_once __DIR__ . '/../models/Product.php';
require_once __DIR__ . '/../models/Account.php';
StaticDB::connect_store();

class CartController
{
    private $model;

    public function __construct()
    {
        // check for persisted item based on user ID
        // if none found, then do this:
        //if ($)
        $this->model = new Cart;
    }

    public function invoke()
    {
        // action: list all cart items
        if (isset($_GET['action'])) {
            $ac = $_GET['action'];
            if (method_exists($this, $ac)) {
                $this->$ac();
            } else {
                // TODO: throw an error
            }
        }
        require __DIR__ . '/../views/CartView.php';
        CartView::render($this->model);
    }

    public function add()
    {
        // if the user is not logged in, they must first log in
        if (!Authentication::is_authenticated()) {
            $login_route = RoutingController::get_action_for('account', 'login');
            header('Location: ' . $login_route);
            die();
        }

        // if nothing was submitted to add to the cart, redirect.
        if (!array_key_exists('product', $_POST) || !is_numeric($_POST['product'])) {
            return;
        }

        $product = Product::get(intval($_POST['product']));
        $this->model->add_product($product);
    }

    public function remove()
    {
        if (!array_key_exists('product', $_POST) || !is_numeric($_POST['product'])) {
            return;
        }

        $product = Product::get(intval($_POST['product']));
        $this->model->remove_product($product);
    }

    public function update()
    {
        if (!array_key_exists('product', $_POST) || !is_numeric($_POST['product'])
            || !array_key_exists('quantity', $_POST) || !is_numeric($_POST['quantity'])) {
            return;
        }

        $product = Product::get(intval($_POST['product']));
        $this->model->set_quantity($product, intval($_POST['quantity']));
    }
}
