<?php
class RoutingController
{
    public static function get_route_for($item, $id = null)
    {
        if (!isset($id)) {
            return RoutingController::get_base() . '?page=' . urlencode($item);
        }

        return RoutingController::get_base() . '?page=' . urlencode($item) . '&show=' . urlencode($id);
    }

    public static function get_base()
    {
        $path_parts = explode('/', $_SERVER['PHP_SELF']);

        if (end($path_parts) == 'index.php') {
            array_pop($path_parts);
            array_push($path_parts, '');
        }

        return implode('/', $path_parts);
    }

    public static function get_action_for($item, $action)
    {
        return RoutingController::get_base() . '?page=' . urlencode($item) . '&action=' . urlencode($action);
    }
}
