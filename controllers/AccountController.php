<?php
require_once __DIR__ . '/../classes/Authentication.php';
require_once __DIR__ . '/RoutingController.php';
require_once __DIR__ . '/../classes/StaticDB.php';

StaticDB::connect_store();

class AccountController
{
    public function invoke()
    {
        if (isset($_GET['show'])) {
            $account = Account::get(intval($_GET['show']));
            // if owner logged in:
            // include __DIR__ . '/../views/AccountAdminView.php';
            // else:
            // include __DIR__ . '/../views/AccountView.php';
            include __DIR__ . '/../views/AccountView.php';
            AccountView::render($account);
        } elseif (isset($_GET['action'])) {
            $ac = $_GET['action'];
            if (method_exists($this, $ac)) {
                $this->$ac();
            } else {
                // TODO: throw an error
            }
        } else {
            // route the user to their account.
            $vid = null;
            if ($vid = Authentication::get_ssid()) {
                header('Location: ' . RoutingController::get_route_for('account', $vid));
                die();
            }
            $this->login(Account::get($vid));
        }
    }

    public function register()
    {
        if (Authentication::is_authenticated()) {
            Page::render("<p>You are already logged in.</p>");
            die();
        }

        $email = '';

        if (!empty($_POST)) {
            $keys = array_keys($_POST);
            $requirements = array('email', 'password');
            $showform = false;

            $account = new Account();

            foreach ($requirements as $req) {
                if (!in_array($req, $keys)) {
                    Page::error("You have submitted a malformed request. Please try again.");
                    $showform = true;
                } else {
                    $showform = false || $showform;
                }
            }

            $email = $_POST['email'];

            // handle the case for duplicate e-mail
            if (Account::get(strtolower($email))) {
                Page::error('Invalid e-mail. The e-mail address used has already been taken!');
                $showform = true;
            }

            // handle the case for invalid e-mail
            if (!Authentication::valid_email($email)) {
                Page::error('Invalid e-mail. The e-mail address used is not a valid e-mail address.');
                $showform = true;
            }

            // handle the case for password too short
            if (strlen($_POST['password']) < 6) {
                Page::error('Password must be at least 6 characters long.');
                $showform = true;
            }

            // handle the case for passwords do not match
            if ($_POST['password'] != $_POST['password-repeat']) {
                Page::error('Passwords must match.');
                $showform = true;
            }

            if (!$showform) {
                $account->email = $email;
                $account->password = $_POST['password'];

                $account->add();

                $this->login($account);
            }
        }

        include __DIR__ . '/../views/RegisterAccountView.php';
        RegisterAccountView::render($email);
    }

    public function login($account = null)
    {
        // TODO: function for login account

        if (array_key_exists('email', $_POST) && array_key_exists('password', $_POST)) {
            $account = Account::get(strtolower($_POST['email']));
            if ($account->authenticate($_POST['password'])) {
                // redirect the user to their own account
                header('Location: ' . RoutingController::get_route_for('account', $account->id));
                die();
            } else {
                Page::error('The credentials entered do not match.');
            }
        }

        include __DIR__ . '/../views/LoginView.php';
        LoginView::render($account);
        // TODO: show the form here

    }

    public function edit()
    {
        // TODO: function for editing user account
    }

    public function logoff()
    {
        // TODO: function for ending session and logging off
        if (isset($_POST['confirm'])) {
            session_destroy();
        }

        $login_route = RoutingController::get_action_for('account', 'login');
        header('Location: ' . $login_route);
    }
}
