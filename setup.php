<?php
require_once __DIR__ . '/classes/Database.php';
require_once __DIR__ . '/views/src/Page.php';

if (!file_exists(__DIR__ . '/classes' . DB_CONFIG)) {
    $config = array();
    if (array_key_exists('database', $_POST)
        && array_key_exists('user', $_POST)
        && array_key_exists('password', $_POST)
        && array_key_exists('server', $_POST)) {
        $config['database'] = $_POST['database'];
        $config['user'] = $_POST['user'];
        $config['password'] = $_POST['password'];
        $config['server'] = $_POST['server'];

        file_put_contents(__DIR__ . '/classes' . DB_CONFIG, json_encode($config));
    } else {
        $htm = <<<HTML
        <form method="post">
            <div class="form-group">
                <label for="server">Database Server IP or Hostname</label>
                <input id="server" class="form-control" type="server" name="text" placeholder="localhost" />
                <p>Please enter the hostname or IP for your SQL server here. (localhost or 127.0.0.1 if it is on the same server)</p>
            </div>
            <div class="form-group">
                <label for="database">Database Name</label>
                <input id="database" class="form-control" type="text" placeholder="your_name_here" name="database" />
                <p>Please enter the name of the empty database you intend to use. If you don't have one, log onto mysql and use the CREATE DATABASE command.</p>
            </div>
            <div class="form-group">
                <label for="username">Database Username</label>
                <input id="username" class="form-control" type="text" placeholder="your_mysql_username" name="user" />
                <p>Please enter the username you use to log into mysql here.</p>
            </div>
            <div class="form-group">
                <label for="password">Database Password</label>
                <input id="password" class="form-control" type="password" name="password" />
                <p>Please enter the password you use to log into mysql here.</p>
            </div>
            <div class="form-group">
                <button class="btn btn-primary">Create Config File</button>
            </div>
        </form>
HTML;
        Page::render($htm);
        die();
    }
}

require_once __DIR__ . '/classes/StaticDB.php';

// connect to the db
StaticDB::connect_store();

// run migrations
// TODO: create migration code

$success = true;

$run_migrations = array();

if (file_exists(__DIR__ . '/migrations/completed.json')) {
    $run_migrations = json_decode(file_get_contents(__DIR__ . '/migrations/completed.json'), true);
}

echo 'Omitting the following migrations as they have already been completed: <br />' . PHP_EOL;
echo implode('<br />' . PHP_EOL, $run_migrations);

$successful = 0;
$justrun = [];

foreach (glob(__DIR__ . '/migrations/*.sql') as $migration) {
    $mig_str = pathinfo($migration, PATHINFO_FILENAME);

    if (array_search($mig_str, $run_migrations) === false) {
        // run the migration

        array_push($justrun, $mig_str);

        $migration_f = fopen($migration, 'r');
        while ($q = fgets($migration_f)) {

            echo 'Processing migration ' . $mig_str . '...<br />' . PHP_EOL;
            $success = StaticDB::$store->conn->query($q);

            if (!$success) {
                // TODO: throw an error
                echo 'Error: query from migration ' . $mig_str . ' failed to execute and here is why: <br />' . PHP_EOL;
                echo "Errno: " . StaticDB::$store->conn->errno . '<br />' . PHP_EOL;
                echo "Error: " . StaticDB::$store->conn->error . '<br />' . PHP_EOL;
            } else {
                array_push($run_migrations, $mig_str);
                $successful++;
                echo 'Migrtion ' . $mig_str . ' successful.<br />' . PHP_EOL;
            }
        }
    }
}

echo '<br />' . ($successful == count($justrun) ? 'All migrations successfully processed :)' : 'There was a problem... only ' . (count($justrun) - $successful) . ' migrations processed successfully.') . '<br />';

file_put_contents(__DIR__ . '/migrations/completed.json', json_encode($run_migrations));
