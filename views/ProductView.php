<?php
require_once __DIR__ . '/../models/Product.php';
require_once __DIR__ . '/src/Page.php';
require_once __DIR__ . '/src/IView.php';
require_once __DIR__ . '/../controllers/RoutingController.php';

class ProductView implements IView
{
    public function render($p)
    {
        $store_link = RoutingController::get_route_for('store');
        $addcart_link = RoutingController::get_action_for('cart', 'add');
        Page::render(<<<HTML
            <div class="product-listing row">
                <div class="product">
                    <h2>{$p->name}</h2>
                    <div class="product-details">
                        <div class="product-image"><img style="max-height: 600px; max-width: 600px;" src="uploads/{$p->image}" alt="{$p->name}" /></div>
                        <div class="product-description">
                            <div class="product-price">{$p->price} <form method="post" action="$addcart_link"><button name="product" value="{$p->id}" class="btn btn-primary">Add to Cart</button></form></div>
                            {$p->description}
                        </div>
                    </div>
                    </div>
                </div>
            </div>
HTML
        );
        die();
    }
}
