<?php
require_once __DIR__ . '/../models/Product.php';
require_once __DIR__ . '/src/Page.php';
require_once __DIR__ . '/../controllers/RoutingController.php';
require_once __DIR__ . '/src/IView.php';
require_once __DIR__ . '/../classes/Authentication.php';
require_once __DIR__ . '/../classes/ImageSizer.php';

class ProductListView implements IView
{
    public function render($product_list)
    {
        Page::render(ProductListView::as_string($product_list));
        die();
    }

    public static function as_string($product_list)
    {
        $plist = '<div class="product-listing row">' . PHP_EOL;
        if (is_array($product_list) && !empty($product_list)) {
            foreach ($product_list as $product) {
                $product_route = RoutingController::get_route_for('product', $product->id);

                $store_link = RoutingController::get_route_for('store');
                $addcart_link = RoutingController::get_action_for('cart', 'add');
                $card_image = new ImageSizer('uploads/' . $product->image, $product->name);

                $delete_route = RoutingController::get_action_for('product', 'delete');
                $delete_button = '';
                if ($product->posted_id == Authentication::get_ssid()) {
                    $delete_button = '<form method="post" action="' . $delete_route . '"><button name="delete" value="' . $product->id . '" class="btn btn-danger">Delete</button></form>';
                }

                if ($product instanceof Product) {
                    $plist .= <<<HTML
                        <div class="card product" style="width: 18rem; margin: 1rem;">
                            <div class="inline-flex justify-content-center">
                                <a href="{$product_route}">
                                    {$card_image->card()}
                                </a>
                            </div>
                            <div class="card-body">
                                <h5 class="card-title"><a href="{$product_route}">{$product->name}</a></h5>
                                <p class="product-price card-text">\${$product->price}</p>
                                <div class="d-flex justify-content-between"><form method="post" action="$addcart_link"><button name="product" value="{$product->id}" class="btn btn-primary">Add to Cart</button></form>
                                $delete_button</div>
                            </div>
                        </div>
HTML;
                }

            }
        } else {
            $plist .= '<p>There are currently no product listings available.</p>';
        }
        $plist .= '</div>';

        if (Authentication::is_authenticated()) {
            $add_link = RoutingController::get_action_for('product', 'add');
            $plist .= <<<HTML
            <div class="admin-menu">
                <a class="btn btn-secondary" href="{$add_link}">Add a product</a>
            </div>
HTML;
        } else {
            $login_link = RoutingController::get_action_for('account', 'login');
            $plist .= <<<HTML
            <div class="menu">
                Please <a href="$login_link">log in</a> to add new product listings.
            </div>
HTML;
        }

        return $plist;
    }
}
