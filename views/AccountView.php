<?php
require_once __DIR__ . '/../models/Account.php';
require_once __DIR__ . '/src/Page.php';
require_once __DIR__ . '/../models/Product.php';
require_once __DIR__ . '/ProductListView.php';
require_once __DIR__ . '/src/IView.php';
require_once __DIR__ . '/../classes/Authentication.php';

class AccountView
{
    public static function render($account)
    {
        $name = htmlspecialchars(substr($account->email, 0, strpos($account->email, '@')));
        $email = htmlspecialchars(str_replace('@', ' [at] ', $account->email));
        $html = '';
        $login_path = RoutingController::get_action_for('account', 'login');
        if ($account->is_authenticated()) {
            // add extra things like:
            // - Order History
            // - Current Cart
            // - Bookmarked items (?)
            // - Email Address

            _Page::$title = "Profile for $name";

            $html = <<<HTML
            <div>
                <h2>Profile for $name</h2>
                <p><strong>E-mail address: </strong>$email</p>
            </div>
HTML;
        } else {
            _Page::$title = "Access Error";
            Page::html_error("You do not have access to view this page. If you normally have access to this page, you may <a href=\"$login_path\">log in</a>.");
        }

        $preamble = '';
        $plist = Product::get_product_list($account->id);
        if (!empty($plist)) {
            $preamble = '<h2>Products listed by ' . $name . '</h2>';
        }
        Page::render($html . $preamble . ProductListView::as_string($plist, $account->id));
    }
}
