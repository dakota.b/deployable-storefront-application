<?php
require_once __DIR__ . '/../models/Product.php';
require_once __DIR__ . '/src/Page.php';
require_once __DIR__ . '/src/IView.php';
require_once __DIR__ . '/../controllers/RoutingController.php';

class AddProductView implements IView
{
    public function render($p = '')
    {
        $store_link = RoutingController::get_route_for('store');
        _Page::$title = "List a New Product";
        Page::render(<<<HTML
            <div>
                <a href="$store_link">Back to store</a>
            </div>
            <h2>List a New Product</h2>
            <form method="post" enctype="multipart/form-data">
                <div class="form-group">
                        <label for="name">Product Name</label>
                        <input type="text" class="form-control" id="name" name="name" aria-describedby="nameHelp" placeholder="Enter product name" />
                </div>
                <div class="form-group">
                        <label for="price">Product Price per Quantity</label>
                        <input type="number" name="price" class="form-control" id="price" placeholder="Enter price" step=".01" />
                </div>
                <div class="form-group">
                        <label for="description">Description</label>
                        <textarea name="description" class="form-control" id="description" placeholder="Enter product description...">
                        </textarea>
                </div>
                <div class="form-group">
                        <label for="image">Product Image
                        <input type="file" name="image" id="image" />
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
HTML
        );
        die();
    }
}
