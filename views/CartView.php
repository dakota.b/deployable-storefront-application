<?php
require_once __DIR__ . '/../models/Product.php';
require_once __DIR__ . '/../models/Cart.php';
require_once __DIR__ . '/src/Page.php';
require_once __DIR__ . '/../classes/ImageSizer.php';
require_once __DIR__ . '/src/IView.php';

class CartView implements IView
{
    public function render($cart)
    {
        $quantity_route = RoutingController::get_action_for('cart', 'update');
        $store_route = RoutingController::get_route_for('store');
        $cart_htm = '<a href="' . $store_route . '">Continue Shopping</a><div class="user-cart">';

        if (count($cart->products) === 0) {
            $cart_htm .= '<h5>Your cart is empty.</h5>';
        } else {

            foreach ($cart->products as $product) {
                $imagesizer = new ImageSizer('uploads/' . $product->product->image, $product->product->name);
                $sfstr = <<<HTML
                <div class="cart-product">
                    <div style="display: flex; align-items: center;">
                    <div class="cart-product-summary" style="display: flex; justify-content: center;flex-direction: column; margin: 20px;">
                        <div class="cart-product-img">
                            {$imagesizer->thumbnail}
                        </div>
                        <div class="cart-product-description">
                            <h3>{$product->product->name}</h3>
                        </div>
                    </div>
                    <form class="cart-product-buttons" action="$quantity_route" method="post" style="margin: 20px; flex-flow: row wrap; display: flex;">
                        <input type="number" name="quantity" class="form-control" value="{$product->quantity}" />
                        <button class="btn btn-primary" name="product" value="{$product->product->id}">Change Quantity</button>
                    </form>
                    <div class="cart-product-price" style="margin: 20px;">
                        \$%0.2f
                    </div>
                </div>
HTML;
                $cart_htm .= sprintf($sfstr, $product->product->price);
            }
            $cart_htm .= sprintf('<p class="subtotal">Subtotal: $<em>%0.2f</em></p>', $cart->get_subtotal());
            $cart_htm .= sprintf('<p class="tax">Tax: $<em>%0.2f</em></p>', $cart->get_tax());
            $cart_htm .= sprintf('<p class="total">Total: $<strong>%0.2f</strong></p>', $cart->get_total());
        }
        $cart_htm .= '</div>';

        Page::render($cart_htm);
        die();
    }
}
