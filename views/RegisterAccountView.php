<?php
require_once __DIR__ . '/../models/Account.php';
require_once __DIR__ . '/src/Page.php';
require_once __DIR__ . '/src/IView.php';
require_once __DIR__ . '/../controllers/RoutingController.php';

class RegisterAccountView implements IView
{
    public function render($email = '')
    {
        Page::render(<<<HTML
            <form method="post">
                <div class="form-group">
                        <label for="email">Email Address</label>
                        <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="my.address@example.com" value="$email" />
                </div>
                <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" name="password" class="form-control" id="password"  />
                </div>
                <div class="form-group">
                        <label for="password-repeat">Repeat Password</label>
                        <input type="password" name="password-repeat" class="form-control" id="password-repeat"  />
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
HTML
        );
        die();
    }
}
