<?php
require_once __DIR__ . '/../models/Account.php';
require_once __DIR__ . '/src/Page.php';
require_once __DIR__ . '/src/IView.php';
require_once __DIR__ . '/../controllers/RoutingController.php';

class LoginView implements IView
{
    public function render($account = null)
    {
        $email = isset($account) ? $account->email : '';

        $form_path = RoutingController::get_action_for('account', 'login');

        _Page::$title = "Log In";

        Page::render(<<<HTML
            <form method="post" action="$form_path">
                <div class="form-group">
                        <label for="email">Email Address</label>
                        <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="my.address@example.com" value="$email" />
                </div>
                <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" name="password" class="form-control" id="password"  />
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
HTML
        );
        die();
    }
}
