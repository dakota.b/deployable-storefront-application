<?php
include_once __DIR__ . '/page_setup.php';
require_once __DIR__ . '/../../controllers/RoutingController.php';
require_once __DIR__ . '/../../classes/Authentication.php';
require_once __DIR__ . '/../../classes/AnchorTag.php';

$home_link = new AnchorTag(RoutingController::get_base(), 'Home');
$store_link = new AnchorTag(RoutingController::get_route_for('store'), 'Store');
$cart_link = new AnchorTag(RoutingController::get_route_for('cart'), 'Cart');
$register_link = new AnchorTag(RoutingController::get_action_for('account', 'register'), 'Register');
?>

<!DOCTYPE html>
<html lang="en-US" enctype="utf-8">
    <head>
        <title><?php echo _Page::$title; ?></title>
        <?php Stylesheet::render_inline(_Page::$style);?>
        <?php
foreach (_Page::$stylesheets as $stylesheet) {
    echo $stylesheet->render();
}
?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    </head>
    <body>
    <nav class="navbar navbar-expand-sm bg-light justify-content-between bg-dark navbar-dark">

        <!-- Links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <?php $home_link->echo_nav();?>
            </li>
            <li class="nav-item">
                <?php $store_link->echo_nav();?>
            </li>
        </ul>

        <ul class="navbar-nav">
            <?php if (Authentication::is_authenticated()) {
    echo <<<HTML
            <li class="nav-item">
                {$cart_link->echo_nav()}
            </li>
HTML;
}
?>
            <li class="nav-item">
                <?php
if (Authentication::is_authenticated()) {
    $login_link = new AnchorTag(RoutingController::get_action_for('account', 'logoff'), 'Log Off');
    $login_link->button_nav('confirm', '1');
} else {
    $login_link = new AnchorTag(RoutingController::get_action_for('account', 'login'), 'Log In');
    $login_link->echo_nav();
    echo '</li><li class="nav-item">';
    $register_link->echo_nav();
}
?>
            </li>
        </ul>

    </nav>
    <div class="container">