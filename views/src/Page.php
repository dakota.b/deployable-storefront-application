<?php
require_once __DIR__ . '/../../classes/_Page.php';

class Page
{
    public static $errors = [];
    public static $notifs = [];

    public static function render($html)
    {
        require_once __DIR__ . '/_header.php';
        foreach (Page::$errors as $error) {
            if (is_string($error)) {
                $t_error = htmlspecialchars($error);
            } else {
                $t_error = $error['msg'];
            }

            echo '<div class="alert alert-danger">
            <strong>Error:</strong> ' . $t_error . '
          </div>';
        }

        foreach (Page::$notifs as $notif) {
            if (is_string($notif)) {
                echo '<div class="alert alert-info">
                    <strong>Notification: </strong> ' . $notif . '
                </div>';
            }
        }
        echo $html;
        require_once __DIR__ . '/_footer.php';
        exit();
    }

    public static function error($errmsg)
    {
        array_push(Page::$errors, $errmsg);
    }

    public static function html_error($errmsg)
    {
        $html_error = array('msg' => $errmsg, 'html' => true);
        Page::error($html_error);
    }

    public static function notify($errmsg)
    {
        array_push(Page::$notifs, $errmsg);
    }
}
