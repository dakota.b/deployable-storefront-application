<?php
include_once 'views/src/page_setup.php';
require_once 'views/src/Page.php';
require_once 'controllers/RoutingController.php';

_Page::$title = "Index";

$actions = array(
    'store' => 'ProductController',
    'account' => 'AccountController',
    'cart' => 'CartController',
    'product' => 'ProductController',
);

$html = '';

if (array_key_exists('page', $_GET) && array_key_exists(strtolower($_GET['page']), $actions)) {
    $ac = $actions[strtolower($_GET['page'])];
    include_once __DIR__ . "/controllers/$ac.php";
    $controller = new $ac;
    $controller->invoke();

} else {
    $html .= '<a href="' . RoutingController::get_route_for('store') . '">Store</a><br />' . PHP_EOL;
    $html .= '<a href="' . RoutingController::get_route_for('account') . '">Account</a><br />' . PHP_EOL;
}

Page::render($html);
