<?php
include_once __DIR__ . '/../classes/StaticDB.php';

class Product
{
    private $id;
    private $name;
    private $price;
    private $description;
    private $image;
    private $posted_id;

    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }

        return $this;
    }

    public function __construct($id = null, $name = null, $price = null, $description = null, $image = null, $poster = null)
    {
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
        $this->description = $description;
        $this->image = $image;
        $this->posted_id = $poster;
    }

    public static function get($pid)
    {
        $product = new Product();
        $stmt = StaticDB::$store->conn->prepare('SELECT `id`, `name`, `price`, `description`, `image`, `posted_id` FROM `products` WHERE `id`=?');
        $stmt->bind_param('s', $pid);
        $stmt->bind_result($id, $name, $price, $description, $image, $posted_id);
        $result = $stmt->execute();
        if ($stmt->fetch()) {
            $product->id = $id;
            $product->name = $name;
            $product->price = $price;
            $product->description = $description;
            $product->image = $image;
            $product->posted_id = $posted_id;
        }

        return $product;
    }

    public function add()
    {
        $stmt = StaticDB::$store->conn->prepare('INSERT INTO `products` (`name`, `price`, `description`, `image`, `posted_id`) VALUES (?, ?, ?, ?, ?)');
        $stmt->bind_param('sdssi', $name, $price, $description, $image, $poster);
        $name = $this->name;
        $poster = $this->posted_id;
        $price = $this->price;
        $description = $this->description;
        $image = $this->image;
        $stmt->execute();
        return StaticDB::$store->conn->insert_id;
        // TODO: fetch ID
        // TODO: Error checking
    }

    public function remove()
    {
        $stmt = StaticDB::$store->conn->prepare('DELETE FROM `products` WHERE `id`=?');
        $stmt->bind_param('i', $id);
        $id = $this->id;
        return $stmt->execute();
    }

    public function update()
    {
        $stmt = StaticDB::$store->conn->prepare('UPDATE `products` SET `name`=?, `price`=?, `description`=?, `image`=? WHERE `id`=?');
        $name = $this->name;
        $price = $this->price;
        $description = $this->description;
        $image = $this->image;
        $id = $this->id;
        $stmt->bind_param('sdssi', $name, $price, $description, $image, $id);
        $stmt->execute();
    }

    // TODO: add filtering options like Price, Popularity, and Date Added
    public static function get_product_list($account_id = null)
    {
        $products = [];
        if ($account_id === null) {
            $stmt = StaticDB::$store->conn->prepare('SELECT `id`, `name`, `price`, `description`, `image`, `posted_id` FROM `products` WHERE 1');
        } else {
            $stmt = StaticDB::$store->conn->prepare('SELECT `id`, `name`, `price`, `description`, `image`, `posted_id` FROM `products` WHERE `posted_id`=?');
            $stmt->bind_param('i', $account_id);
        }
        if ($stmt) {
            $stmt->bind_result($id, $name, $price, $description, $image, $posted_id);
            $stmt->execute();

            while ($stmt->fetch()) {
                $prod = new Product($id, $name, $price, $description, $image);
                $prod->posted_id = $posted_id;

                array_push($products, $prod);
            }

            return $products;
        }

        return array();
    }

    public function owned_by(Account $acc)
    {
        if (intval($acc->id) == intval($this->posted_id) || $acc->access_level > 1) {
            return true;
        }

        return false;
    }

    public function search_products($term)
    {
        $products = [];
        $stmt = StaticDB::$store->conn->prepare("SELECT `id`, `name`, `price`, `description`, `image` FROM `products` WHERE (`name` LIKE ? OR `description` LIKE ?)");
        $term = "%$term%";
        $stmt->bind_param('ss', $term, $term);
        $result = $stmt->execute();
        if ($result->num_rows > 0) {
            while ($result->fetch()) {
                $prod = new Product;
                $product->id = $id;
                $product->name = $name;
                $product->price = $price;
                $product->description = $description;
                $product->image = $image;
                array_push($products, $product);
            }
        }

        return $products;
    }
}
