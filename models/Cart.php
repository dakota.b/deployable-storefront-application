<?php
include_once __DIR__ . '/CartProduct.php';
include_once __DIR__ . '/../classes/StaticDB.php';
include_once __DIR__ . '/../classes/Authentication.php';

class Cart
{
    private $user;
    private $products = [];

    public function __construct(Account $acc = null)
    {
        if (isset($u)) {
            $this->user = $acc;
        } else {
            $this->user = Account::get(Authentication::get_ssid());
        }

        $stmt = StaticDB::$store->conn->prepare("SELECT
                `cartproducts`.`id`,
                `products`.`id`,
                `products`.`name`,
                `products`.`price`,
                `products`.`description`,
                `cartproducts`.`quantity`,
                `products`.`image`,
                `products`.`posted_id`
            FROM `cartproducts`
            LEFT JOIN `products`
                ON `cartproducts`.`product_id`=`products`.`id`
            WHERE `cartproducts`.`account_id`=?");
        $accid = $this->user->id;
        $stmt->bind_param('i', $accid);
        $stmt->bind_result($db_id, $id, $name, $price, $description, $quantity, $image, $posted_id);
        $result = $stmt->execute();
        while ($stmt->fetch()) {
            $cartprod = new CartProduct($this->user);
            $cartprod->id = $db_id;
            $cartprod->product->id = $id;
            $cartprod->quantity = intval($quantity);
            $cartprod->product->name = $name;
            $cartprod->product->price = $price;
            $cartprod->product->description = $description;
            $cartprod->product->image = $image;
            $cartprod->product->posted_id = $posted_id;
            $cartprod->account = Authentication::get_ssid();
            if ($cartprod->product->id) {
                $this->add_cart_product($cartprod);
            }
        }
    }

    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }

        return $this;
    }

    public function add_product(Product $p)
    {
        // first check if this product is already in the cart
        $found = false;
        foreach ($this->products as $prod) {
            if ($prod->product->id == $p->id) {
                // product found! increase its quantity and update the db
                $prod->quantity++;
                $prod->update();
                $found = true;
            }
        }

        if (!$found) {
            $cart_prod = CartProduct::get_or_create($p, $this->user);
            $this->add_cart_product($cart_prod);
        }
    }

    public function remove_product(Product $p)
    {
        $cart_prod = CartProduct::get($p, $this->user);
        foreach ($this->products as $k => $prod) {
            if ($prod->product->id == $p->id) {
                array_splice($this->products, $k, 1);
            }
        }
        $cart_prod->remove();
    }

    public function set_quantity(Product $p, $q)
    {
        // if quantity has been dropped to 0, delete the product
        if ($q == 0) {
            $this->remove_product($p);
            return true;
        }

        foreach ($this->products as $product) {
            if ($product->product->id == $p->id) {
                $product->quantity = intval($q);
                $product->update();
                return true;
            }
        }

        return false;
    }

    public function get_subtotal()
    {
        $subtotal = 0.0;

        foreach ($this->products as $prod) {
            $subtotal += $prod->subtotal;
        }

        return $subtotal;
    }

    public function get_tax()
    {
        return 0.06 * $this->get_subtotal();
    }

    public function get_total()
    {
        return $this->get_subtotal() + $this->get_tax();
    }

    public function add_cart_product(CartProduct $prod)
    {
        array_push($this->products, $prod);
    }

    public static function get_user_cart(Account $acc = null)
    {
        $cart = new Cart($acc);

        return $cart;
    }
}
