<?php
require_once 'Product.php';
require_once 'Account.php';
require_once __DIR__ . '/../classes/StaticDB.php';

class CartProduct
{
    private $quantity = 0;
    private $id;
    private $account;
    private $product;

    public function __construct($account = null, $product = null)
    {
        if (!isset($account)) {
            $account = Account::get();
        }

        $this->account = $account;

        if (isset($product)) {
            $this->product = $product;
        } else {
            $this->product = new Product;
        }

    }

    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }

        if ($property == 'subtotal') {
            return $this->get_subtotal();
        }
    }

    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }

        return $this;
    }

    private function get_subtotal()
    {
        return $this->quantity * $this->product->price;
    }

    public static function get_or_create(Product $prod, Account $account)
    {
        $product = null;

        if (!($product = CartProduct::get($prod, $account))) {
            $product = CartProduct::from_product($prod);
            $product->account = $account;
            $product->add();
        }

        return $product;
    }

    public function remove()
    {
        $stmt = StaticDB::$store->conn->prepare('DELETE FROM `cartproducts` WHERE `id`=?');
        $stmt->bind_param('i', $this->id);
        $stmt->execute();
    }

    public static function get(Product $prod, Account $account)
    {
        $product = new CartProduct;
        $stmt = StaticDB::$store->conn->prepare('SELECT
                `cartproducts`.`id`,
                `cartproducts`.`account_id`,
                `products`.`id` AS `product_id`,
                `products`.`name`,
                `products`.`price`,
                `products`.`description`,
                `cartproducts`.`quantity`
            FROM `cartproducts`
                LEFT JOIN `products`
                ON `products`.`id`=`cartproducts`.`product_id`
            WHERE `cartproducts`.`account_id`=?
                AND `cartproducts`.`product_id`=?');
        $accid = $account->id;
        $prodid = $prod->id;
        if (!$stmt) {
            die(StaticDB::$store->conn->error);
        }

        $stmt->bind_param('ii', $accid, $prodid);
        $stmt->bind_result($id, $account_id, $product_id, $name, $price, $description, $quantity);
        $stmt->execute();
        if ($stmt->fetch()) {
            $product->product->name = $name;
            $product->product->price = $price;
            $product->product->description = $description;
            $product->product->quantity = $quantity;
            $product->product->id = $product_id;
            $product->account = $account_id;
            $product->id = $id;
            return $product;
        }

        return false;
    }

    public function update()
    {
        $stmt = StaticDB::$store->conn->prepare('UPDATE `cartproducts` SET `product_id`=?, `account_id`=?, `quantity`=? WHERE `id`=?');
        $pid = $this->product->id;
        $aid = $this->account;
        $quantity = $this->quantity;
        $id = $this->id;
        $stmt->bind_param('iiii', $pid, $aid, $quantity, $id);
        $stmt->execute();
    }

    public function add()
    {
        $insert_stmt = StaticDB::$store->conn->prepare('INSERT INTO `cartproducts` (`product_id`, `account_id`, `quantity`) VALUES (?, ?, 1)');
        $pid = $this->product->id;
        $aid = $this->account->id;
        $insert_stmt->bind_param('ii', $pid, $aid);
        $insert_stmt->execute();
        $this->id = StaticDB::$store->conn->insert_id;
        echo StaticDB::$store->conn->error;
    }

    public static function from_product(Product $prod)
    {
        $p = new CartProduct;
        $p->product = $prod;
        $p->quantity = 1;
        $p->account_id = Authentication::get_ssid();
        return $p;
    }
}
