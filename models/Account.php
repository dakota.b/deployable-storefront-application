<?php
session_start();
require_once __DIR__ . '/../classes/StaticDB.php';

class Account
{
    private $passwordHash;
    private $email;
    private $ssid;
    private $stripeId;
    private $id;
    private $isAuthenticated = false;
    private $access_level;

    public function authenticate($password)
    {
        // set $isAuthenticated based on password_verify()
        // set session variables
        if (password_verify($password, $this->passwordHash)) {
            // set session variables
            $this->isAuthenticated = true;
            $_SESSION['ssid'] = $this->id;
            return true;
        } else {
            $this->isAuthenticated = false;
            session_abort();
            return false;
        }
    }

    public function is_authenticated()
    {
        $ssid = 'no';

        if (array_key_exists('ssid', $_SESSION)) {
            $ssid = $_SESSION['ssid'];
        }

        $valid_ssid = $ssid == $this->id; // temporary
        return $this->isAuthenticated || ($this->isAuthenticated = $valid_ssid);
    }

    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function update()
    {
        if (isset($this->id)) {
            $stmt = StaticDB::$store->$conn->prepare('UPDATE `accounts` SET `email`=?, `password`=?, `ssid`=?, `stripe_id`=? WHERE `id`=?');
            $stmt->bind_param('ssssi', $this->email, $this->passwordHash, $this->ssid, $this->stripeId, $this->id);
            return $stmt->execute();
        }

        return false;
    }

    public function add()
    {
        if (isset($this->email) && isset($this->passwordHash)) {
            $stmt = StaticDB::$store->conn->prepare('INSERT INTO `accounts` (`email`, `password`) VALUES (?, ?)');
            $email = $this->email;
            $pass = $this->passwordHash;
            $stmt->bind_param('ss', $email, $pass);
            $stmt->execute();
            $this->id = StaticDB::$store->conn->insert_id;
            return $this->id;
        }
    }

    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }

        // handle encrypting the password here
        if ($property == 'password') {
            $this->passwordHash = password_hash($value, PASSWORD_BCRYPT);
        }

        return $this;
    }

    public static function get($prim = null)
    {
        if (!isset($prim)) {
            $prim = intval(Authentication::get_ssid());
        }

        $acct = new Account;

        if (is_numeric($prim)) {
            $stmt = StaticDB::$store->conn->prepare('SELECT `id`, `email`, `ssid`, `stripe_id`, `password` FROM `accounts` WHERE `id`=?');

            if ($stmt) {
                $stmt->bind_param('i', $prim);
            } else {
                echo StaticDB::$store->conn->error;
            }

        } else {
            $stmt = StaticDB::$store->conn->prepare('SELECT `id`, `email`, `ssid`, `stripe_id`, `password` FROM `accounts` WHERE `email`=?');
            $stmt->bind_param('s', $prim);
        }

        $stmt->bind_result($id, $email, $ssid, $stripeId, $password);
        $result = $stmt->execute();
        if ($stmt && $stmt->fetch() && $stmt->close()) {
            $acct->id = $id;
            $acct->email = $email;
            $acct->ssid = $ssid;
            $acct->stripeId = $stripeId;
            $acct->passwordHash = $password;
            return $acct;
        }

        return false;
    }
}
